<?php

require "PasswordStorage.php";
include 'config.inc.php';

class User
{
        public $username;
        public $token;
        public $permission_role;
	public $email;
}

// Check whether username or password is set from android
if(isset($_POST['username']) && isset($_POST['password']))
{
	$username = $_POST['username'];
      	$password = $_POST['password'];

	$stmt = $conn->prepare("SELECT hashed_password,permission_role,salt,id,email FROM users WHERE username = ?;"); 
	$stmt->execute(array($username));
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC); 
	foreach($result as $k=> $v) { 
		$email = $v['email'];
        	$id =  $v['id'];
		$salt =  $v['salt'];
		$passwordRaw = $v['hashed_password'];
		$role = $v['permission_role'];
   	}

	$prepared_hash = "sha1:64000:18:".$salt.":".$passwordRaw;

	$answerObj = new User;

	$password_correct = PasswordStorage::verify_password($password,$prepared_hash);
	if($password_correct == true)
	{
		$answerObj->username = $username;
		$answerObj->permission_role = $role;
		$answerObj->email = $email;
		$token = bin2hex(random_bytes(16));
				
		$stmt = $conn->prepare("UPDATE users SET token='".$token."' WHERE username = ?;");
	        $stmt->execute(array($username));
		$answerObj->token = $token;
	}
	else
	{
		$answerObj->email = "";
		$answerObj->username = "";
                $answerObj->permission_role = "";
		$answerObj->token = "";
	}
	$jsonStr = json_encode($answerObj);
	echo $jsonStr;
}
?>
