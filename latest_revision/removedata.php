<?php

require("config.inc.php");

$act = $_GET['del'];
$token = $_GET['token'];
$element_id = $_GET['id'];
if(isset($token) && $token != "")
{
	$stmt = $conn->prepare("SELECT id FROM users WHERE token = ?;");
	$stmt->execute(array($token));
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($result as $k=> $v) {
		$id =  $v['id'];
	}
	
	$objArr = array();

	switch($act)
	{
		case "note":
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $sql = "DELETE FROM notes WHERE id = ? AND userid = ?;";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute(array($element_id,$id));
			echo "Record deleted successfully";
			break;
		case "timetable":
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $sql = "DELETE FROM timetable WHERE id = ? AND userid = ?;";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute(array($element_id,$id));
                        echo "Record deleted successfully";
			break;
		case "calendar":
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $sql = "DELETE FROM calendar WHERE id = ? AND userid = ?;";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute(array($element_id,$id));
                        echo "Record deleted successfully";
			break;
	}
}
?>
