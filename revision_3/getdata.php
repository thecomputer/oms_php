<?php

require("config.inc.php");

class User
{
        public $username;
        public $password;
        public $email;
	public $permission_role;
	public $token;
}
class Note
{
	public $id;
	public $title;
	public $description;
	public $timestamp;
}

class CalendarEvent
{
	public $id;
	public $title;
	public $description;
	public $notes;
	public $timestamp;
}

class TimetableEvent
{
	public $id;
	public $day;
	public $hour;
	public $minute;
	public $title;
	public $description;
	public $notes;
}

$act = $_GET['get'];
$token = $_GET['token'];
if(isset($token) && $token != "")
{
	$stmt = $conn->prepare("SELECT id FROM users WHERE token = ?;");
	$stmt->execute(array($token));
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($result as $k=> $v) {
		$id =  $v['id'];
	}
	
	$objArr = array();

	switch($act)
	{
		
		case "note":
			$orderBy = $_GET['orderBy'];
			$sort = $_GET['sort'];

			if($orderBy == "DESC")
			{
				if($sort == "title")
				{
					$sql = "SELECT * FROM notes WHERE userid = ? ORDER BY title DESC;";
				}
				else {
					if($sort == "unixtimestamp")
						$sql = "SELECT * FROM notes WHERE userid = ? ORDER BY unixtimestamp DESC;";
				}
			}
			else
			{
				if($sort == "title") {
                                        $sql = "SELECT * FROM notes WHERE userid = ? ORDER BY title ASC;";
                                }
				else { 
					if($sort == "unixtimestamp")
                                        	$sql = "SELECT * FROM notes WHERE userid = ? ORDER BY unixtimestamp ASC;";     
				}
			}
			
			$stmt = $conn->prepare($sql);
		        $stmt->execute(array($id));

			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$i=0;
			foreach($result as $k=> $v) {
				$noteObj = new Note;
				$noteObj->title = $v['title'];
				$noteObj->id = $v['id'];
				$noteObj->description = $v['description'];
				$noteObj->timestamp = $v['unixtimestamp'];
	                        $objArr[$i] = $noteObj;
				$i++;
        		}

			$objJSON = json_encode($objArr);
			echo $objJSON;	
			break;
		case "timetable":
			$day = $_GET['day'];
			if(isset($_GET['day']) && $day != "")
			{
				$stmt = $conn->prepare("SELECT * FROM timetable WHERE userid = ? AND day = ? ORDER BY hour,minute ASC;");
                        	$stmt->execute(array($id,$day));

                        	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

                        	$i=0;
                        	foreach($result as $k=> $v) {
                        	        $timetableObj = new TimetableEvent;
                        	        $timetableObj->title = $v['title'];
                        	        $timetableObj->id = $v['id'];
                        	        $timetableObj->description = $v['description'];
                        	        $timetableObj->notes = $v['notes'];
                        	        $timetableObj->day = $v['day'];
                        	        $timetableObj->hour = $v['hour'];
                        	        $timetableObj->minute = $v['minute'];
                        	        $objArr[$i] = $timetableObj;
                       		        $i++;
                        	}
			
			}
			$objJSON = json_encode($objArr);
                        echo $objJSON;

			break;

		case "calendar":
			$stmt = $conn->prepare("SELECT * FROM calendar WHERE userid = ? ORDER BY unixtimestamp DESC;");
                        $stmt->execute(array($id));

                        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

                        $i=0;
                        foreach($result as $k=> $v) {
                                $calendarObj = new CalendarEvent;
                                $calendarObj->id = $v['id'];
                                $calendarObj->title = $v['title'];
                                $calendarObj->description = $v['description'];
                                $calendarObj->notes = $v['notes'];
                                $calendarObj->timestamp = $v['unixtimestamp'];
                                $objArr[$i] = $calendarObj;
                                $i++;
                        }

                        $objJSON = json_encode($objArr);
                        echo $objJSON;
			break;
		case "note_filtered":
			case "note":
			$orderBy = $_GET['orderBy'];
			$sort = $_GET['sort'];

			if($orderBy == "DESC")
			{
				if($sort == "title")
				{
					$sql = "SELECT * FROM notes WHERE userid = ? AND unixtimestamp >= ? ORDER BY title DESC;";
				}
				else {
					if($sort == "unixtimestamp")
						$sql = "SELECT * FROM notes WHERE userid = ? AND unixtimestamp >= ? ORDER BY unixtimestamp DESC;";
				}
			}
			else
			{
				if($sort == "title") {
                                        $sql = "SELECT * FROM notes WHERE userid = ? AND unixtimestamp >= ? ORDER BY title ASC;";
                                }
				else { 
					if($sort == "unixtimestamp")
                                        	$sql = "SELECT * FROM notes WHERE userid = ? AND unixtimestamp >= ? ORDER BY unixtimestamp ASC;";     
				}
			}
			
			$stmt = $conn->prepare($sql);
		        $stmt->execute(array($id,$_GET['start_timestamp']));

			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$i=0;
			foreach($result as $k=> $v) {
				$noteObj = new Note;
				$noteObj->title = $v['title'];
				$noteObj->id = $v['id'];
				$noteObj->description = $v['description'];
				$noteObj->timestamp = $v['unixtimestamp'];
	                        $objArr[$i] = $noteObj;
				$i++;
        		}

			$objJSON = json_encode($objArr);
			echo $objJSON;	
			break;			

		case "calendar_filtered":
			
                        $stmt = $conn->prepare("SELECT * FROM calendar WHERE userid = ? AND unixtimestamp >= ? AND unixtimestamp <= ? ORDER BY unixtimestamp DESC;");
                        $stmt->execute(array($id,$_GET['start_timestamp'],$_GET['stop_timestamp']));

                        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

                        $i=0;
                        foreach($result as $k=> $v) {
                                $calendarObj = new CalendarEvent;
                                $calendarObj->id = $v['id'];
                                $calendarObj->title = $v['title'];
                                $calendarObj->description = $v['description'];
                                $calendarObj->notes = $v['notes'];
                                $calendarObj->timestamp = $v['unixtimestamp'];
                                $objArr[$i] = $calendarObj;
                                $i++;
                        }

                        $objJSON = json_encode($objArr);
                        echo $objJSON;
                        break;
	}
}


?>
