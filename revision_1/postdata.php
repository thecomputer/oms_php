<?php

require("config.inc.php");

$act = $_GET['set'];
$token = $_GET['token'];
if($_SERVER["REQUEST_METHOD"] == "POST")
{
	if(isset($token))
	{
		$stmt = $conn->prepare("SELECT id FROM users WHERE token = ?;"); 
		$stmt->execute(array($token));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC); 
		foreach($result as $k=> $v) { 
        		$id =  $v['id'];
   		}

		switch ($act)
		{
			case "note":
				$title = $_POST['title'];
				$desc = $_POST['desc'];
				$timestamp = time();
		
				try {
					$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$sql = "INSERT INTO notes VALUES(NULL,".$id.",? ,? ,?);";
					$stmt = $conn->prepare($sql);
					$stmt->execute(array($title,$desc,$timestamp));
					echo "New record created successfully";
				}
				catch(PDOException $e)
				{
					echo $sql . "<br>" . $e->getMessage();
				}

				break;

			case "timetable_event":
                                $title = $_POST['title'];
                                $desc = $_POST['desc'];
				$note = $_POST['notes'];
				$day = $_POST['day'];
				$hour = $_POST['hour'];
				$minute = $_POST['minute'];

                                try {
                                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                        $sql = "INSERT INTO timetable VALUES(NULL,".$id.",? ,? ,? ,? ,? ,?);";
                                        $stmt = $conn->prepare($sql);
                                        $stmt->execute(array($title,$desc,$note,$day,$hour,$minute));
                                        echo "New record created successfully";
                                }
                                catch(PDOException $e)
                                {
                                        echo $sql . "<br>" . $e->getMessage();
                                }

				break;

			case "calendar_event":
				$title = $_POST['title'];
                                $desc = $_POST['desc'];
                                $note = $_POST['notes'];
				$timestamp = $_POST['timestamp'];

                                try {
                                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                        $sql = "INSERT INTO calendar VALUES(NULL,".$id.",? ,? ,? ,?);";
                                        $stmt = $conn->prepare($sql);
                                        $stmt->execute(array($title,$desc,$note,$timestamp));
                                        echo "New record created successfully";
                                }
                                catch(PDOException $e)
                                {
                                        echo $sql . "<br>" . $e->getMessage();
                                }

				break;
		}	

	}
}

?>
