<?php

include 'config.inc.php';

function hash_pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output = false) {

        class pbkdf2 {
            public $algorithm;
            public $password;
            public $salt;
            public $count;
            public $key_length;
            public $raw_output;

            private $hash_length;
            private $output         = "";

            public function __construct($data = null)
            {
                if ($data != null) {
                    $this->init($data);
                }
            }

            public function init($data)
            {
                $this->algorithm  = $data["algorithm"];
                $this->password   = $data["password"];
                $this->salt       = $data["salt"];
                $this->count      = $data["count"];
                $this->key_length = $data["key_length"];
                $this->raw_output = $data["raw_output"];
            }

            public function hash()
            {
                $this->algorithm = strtolower($this->algorithm);
                if(!in_array($this->algorithm, hash_algos(), true))
                    throw new Exception('PBKDF2 ERROR: Invalid hash algorithm.');

                if($this->count <= 0 || $this->key_length <= 0)
                    throw new Exception('PBKDF2 ERROR: Invalid parameters.');

                $this->hash_length = strlen(hash($this->algorithm, "", true));
                $block_count = ceil($this->key_length / $this->hash_length);
                for ($i = 1; $i <= $block_count; $i++) {
                    // $i encoded as 4 bytes, big endian.
                    $last = $this->salt . pack("N", $i);
                    // first iteration
                    $last = $xorsum = hash_hmac($this->algorithm, $last, $this->password, true);
                    // perform the other $this->count - 1 iterations
                    for ($j = 1; $j < $this->count; $j++) {
                        $xorsum ^= ($last = hash_hmac($this->algorithm, $last, $this->password, true));
                    }
                    $this->output .= $xorsum;
                    if($this->raw_output)
                        return substr($this->output, 0, $this->key_length);
                    else
                        return bin2hex(substr($this->output, 0, $this->key_length));
                }
            }
        }
}

	 // Check whether username or password is set from android	
     if(isset($_GET['username']) && isset($_GET['password']))
     {


		$username = $_GET['username'];
          	$password = $_GET['password'];

		$stmt = $conn->prepare("SELECT salt,id FROM users WHERE username = ?;"); 
		$stmt->execute(array($username));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC); 
		foreach($result as $k=> $v) { 
        		$id =  $v['id'];
			$salt =  $v['salt'];
   		}

		


		$iterations = 65536;
		$key_length = 128;
		
try {
		$hashedPassword = hash_pbkdf2("sha1", $password, $salt, $iterations, $key_length, false);
}
catch (Exception $e)
{
 echo "error";
}
				echo $salt."<br>";
				echo "<br>".$hashedPassword;

		  // Innitialize Variable
		  $result='';
		  // Query database for row exist or not
          $sql = 'SELECT * FROM users WHERE  username = :username AND hashed_password = :password';
          $stmt = $conn->prepare($sql);
          $stmt->bindParam(':username', $username, PDO::PARAM_STR);
          $stmt->bindParam(':password', $hashedPassword, PDO::PARAM_STR);
          $stmt->execute();
          if($stmt->rowCount())
          {
			 $result="true";	
          }  
          elseif(!$stmt->rowCount())
          {
			  	$result="false";
          }
		  
		  // send result back to android
   		  echo $result;
  	}

?>
